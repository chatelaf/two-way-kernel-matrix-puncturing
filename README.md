# Two-way kernel matrix puncturing

This is the companion gitlab repository for our [arXiv manuscript](https://arxiv.org/abs/2102.12293) _"Two-way kernel matrix puncturing: towards resource-efficient PCA and spectral clustering"_ by Romain Couillet, Florent Chatelain and Nicolas Le Bihan.

## Supplementary materials

### Proofs

The elements of  proof of the main theorems in the core article are detailed in the pdf file
[supp_mat_proofs.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/two-way-kernel-matrix-puncturing/-/blob/master/supp_mat_proofs.pdf)

### GAN Data

[VGG](https://arxiv.org/pdf/1409.1556.pdf) features of randomly [BigGAN](https://arxiv.org/abs/1809.11096)-generated images that we use in the paper are stored as [Git LFS](https://github.com/git-lfs/git-lfs/wiki) objects which avoids storing them locally. However this is possible to store the corresponding `.npy` files locally in order to avoid downloading them each time they are processed in the codes. This can be done manually via the GitLab web interface. Alternatively, this can be done, once Git LFS is [installed](https://github.com/git-lfs/git-lfs/wiki/Installation), with the console command line:
```sh
git lfs pull
```

### Code

The python jupyter notebook [supp_mat_figures.ipynb](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/two-way-kernel-matrix-puncturing/-/blob/master/supp_mat_figures.ipynb) is set up to replicate the figures shown in the paper.
This loads the local file [punctutils.py](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/two-way-kernel-matrix-puncturing/-/blob/master/punctutils.py) which defines the user's function module. The python modules needed to run the notebook include notably `seaborn` for plotting or `tensorflow`  for loading the MNIST-fashion dataset (Figs 6-8).

This notebook can be run interactively and remotely (online) using the _mybinder_ service: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Ftwo-way-kernel-matrix-puncturing/master?urlpath=lab/tree/supp_mat_figures.ipynb) (open the link and wait a few seconds for the environment to load).


#### Requirements

The processing of GAN data requires at least  32GB of RAM.
