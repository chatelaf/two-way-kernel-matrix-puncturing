# coding: utf-8
""" Two-way puncturing utilities to prepare and puncture synthetic or real-word
    data. Also comprise functions to display spectral information, limiting
    population spikes, clustering performances, ... as the figures in the paper.
"""

import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
import scipy.linalg as lin
import scipy.stats as stats
import scipy.sparse.linalg
import scipy.special
import sys
import pandas as pd
import seaborn as sns

from tensorflow.keras.datasets import fashion_mnist

from numpy.random import default_rng
rng = default_rng(0)


def puncture_X(X, eps):
    """Puncture the data matrix X with a selection rate eps"""

    (p, n) = X.shape
    cmpl = False
    if eps > 0.5:
        cmpl = True
        eps = 1 - eps

    # Compute the (asymptotic) equivalent selection rate when we sample with
    # replacement.
    if eps <= 0:
        q = 0
    else:
        q = -np.log(1 - eps)

    # Number of entries in the lower triangular matrix B
    nedges = p * n
    # Sample the entry indexes to retain
    indreplace = rng.choice(nedges, replace=True, size=int(np.round(q * nedges)))

    # Remove duplicates from the sampled indices
    if eps > 0.01:
        if not cmpl:
            ind = -np.ones(nedges, dtype=int)
            ind[indreplace] = indreplace
            ind = ind[ind >= 0]
    else:
        ind = np.unique(indreplace)

    if cmpl:
        ind = np.arange(nedges)
        ind[indreplace] = -1
        ind = ind[ind >= 0]

    # Create the sparsified data matrix
    vals = X.ravel()[ind]
    I, J = np.unravel_index(ind, X.shape)
    X_S = sp.sparse.coo_matrix((vals, (I, J)), shape=X.shape)
    # get the 'csc' representation for efficient column slicing
    X_S = X_S.tocsc()
    return X_S


def ind2sub4low(IND):
    """Subscripts from linear index for lower triangular matrix (only
    elements below diagonal). This determines the equivalent subscript
    values corresponding to a given single index into a 2D lower
    triangular matrix, excluded all elements over the diagonal.
    """

    I = np.round(np.floor(-0.5 + 0.5 * np.sqrt(1 + 8 * IND)) + 2).astype("int") - 1
    J = np.round((I + 1) * (2 - I) / 2 + IND).astype("int") - 1
    return I, J


def mask_B(n, eps, is_diag=1):
    """Create the sparsity mask with density eps for the kernel matrix.
    The diagonal entries are omited when is_diag is zero
    """

    cmpl = False
    if eps > 0.5:
        cmpl = True
        eps = 1 - eps

    # compute the (asymptotic) equivalent selection rate when we sample with replacement
    if eps <= 0:
        q = 0
    else:
        q = -np.log(1 - eps)

    q = -np.log(1 - eps)
    # number of entries in the lower triangular matrix B
    nedges = (n * (n - 1)) // 2
    # sample the entry indexes to retain
    indreplace = rng.choice(nedges, replace=True, size=int(np.round(q * nedges)))

    if cmpl:
        ind = np.arange(nedges)
        ind[indreplace] = -1
        ind = ind[ind > 0]
    else:
        ind = indreplace

    # no need to remove the duplicates (done by the sparse matrix factory)
    # create the sparse selection matrix
    data = np.ones(len(ind))
    I, J = ind2sub4low(ind)
    B = sp.sparse.coo_matrix((data, (I, J)), shape=(n, n))

    # Symmetric matrix (no need to store the upper diag triangular array)
    if is_diag != 0:
        B += sp.sparse.eye(n)
    B = B.tocsr()

    # remove duplicate (possible du to csr conversion)
    B.data[B.data > 1] = 1
    indices = B.indices
    indptr = B.indptr
    return B, indices, indptr


def puncture_K_vanilla(X_S, B):
    """Compute the punctured kernel matrix with spasity mask B"""

    p = X_S.shape[0]
    if sp.sparse.issparse(X_S):
        X_S = X_S.todense()
    K = X_S.T @ X_S
    K = sp.sparse.csr_matrix(B.multiply(K)) / p
    K = K + sp.sparse.tril(K, k=-1).T
    return K


def gen_synth_mus(p, n, cov_mu):
    """Draw the two correlated mean vectors for a two-classes model"""

    c0 = p / n  # dimension/sample size ratio
    # set the corr coeff between the mean vectors
    rho = cov_mu[0, 1] / np.sqrt(cov_mu[0, 0] * cov_mu[1, 1])
    # draw the mean vectors mu_1,2
    mu0 = rng.normal(size=(p,))
    mu1 = np.sqrt((1 - rho ** 2)) * rng.normal(size=(p,)) + rho * mu0
    mu0 = mu0 * np.sqrt(cov_mu[0, 0])
    mu1 = mu1 * np.sqrt(cov_mu[1, 1])
    mus = np.concatenate((mu0, mu1)).reshape(p, 2)
    return mus


def gen_synth_X(p, n, mus, cs):
    """Draw the noisy data matrix X and get the population spike matrices"""

    # Repmat the mean vectors for each of the n samples and stack them in P
    # using the proportion cs for each class
    n0 = int(cs[0] * n)
    J = np.zeros((n, 2))
    J[:n0, 0] = 1
    J[n0:, 1] = 1
    P = mus @ J.T
    M = np.diag(np.sqrt(cs)) @ (mus.T @ mus) @ np.diag(np.sqrt(cs))
    # Population spikes
    ells, vM = sp.linalg.eigh(M)

    # Full data matrix
    Z = rng.normal(size=(p, n))
    X = Z + P
    return X, ells, vM


def puncture_eigs(X, eB, eS, b=1, sparsity=0):
    """Make the simulation to get the spectral data for a two-way punctured
    kernel matrix from a data matrix X.
    """

    X_S = puncture_X(X, eS)
    B, _, _ = mask_B(X.shape[1], eB, is_diag=b)
    K = puncture_K_vanilla(X_S, B)

    if sparsity:
        lambdas, U = sp.sparse.linalg.eigsh(
            K, k=sparsity, which="LA", tol=0, return_eigenvectors=True
        )
    else:
        lambdas = np.linalg.eigvalsh(K.todense())
        U = None
    return lambdas, U




def gen_MNIST_vectors(n0=1024):
    """Load, reshape and preprocess and  the 'trouser' and 'pullover'
    data images
    """

    n1 = n0
    (X, y), _ = fashion_mnist.load_data()
    selected_labels = [1, 2]  # trouser and pullover
    X0 = X[y == selected_labels[0]].reshape(-1, 28 ** 2)
    X1 = X[y == selected_labels[1]].reshape(-1, 28 ** 2)

    mean0 = X0.mean()
    mean1 = X1.mean()

    data_full = np.concatenate((X0, X1), axis=0).astype(float)  # (n,p) convention
    data_full = X.reshape(-1, 28 ** 2).astype(float)
    mean_full = data_full.mean(axis=0)
    sd_full_iso = data_full.ravel().std(axis=0, ddof=1)

    ind0 = rng.choice(X0.shape[0], replace=False, size=n0)
    X0 = X0[ind0, :]
    ind1 = rng.choice(X1.shape[0], replace=False, size=n1)
    X1 = X1[ind1, :]

    data = np.concatenate((X0, X1), axis=0).astype(float)  # (n,p) convention
    n, p = data.shape
    c = p / n

    X = (data - data.mean(axis=0)) / sd_full_iso

    X = X.T  # now this is (p,n) convention
    # mean0 / sd_full_iso, mean1 / sd_full_iso
    ell = (((mean0 - mean1) / sd_full_iso) ** 2) * p
    return X, n0, n1, ell

def gen_GAN_vectors(n0=5000):
    """Load, reshape and preprocess and the VGG feature vectors for the
    'tabby' and 'collie' BigGAN images
    """
    n1 = n0

    from pathlib import Path
    basename = Path('./data') # relative path to data folder in the gitlab repo
    infile0 = 'GAN_collie_vgg19_large.npy'
    X0 = np.load((basename / infile0).as_posix())
    infile1 = 'GAN_tabby_vgg19_large.npy'
    X1 = np.load((basename / infile1).as_posix())

    data_full = np.concatenate((X0, X1), axis=0).astype(float)  # (n,p) convention
    sd_full = data_full.std(axis=0, ddof=1)

    X0 = X0[:n0, :]
    X1 = X1[:n1, :]

    data = np.concatenate((X0, X1), axis=0).astype(float)  # (n,p) convention
    n, p = data.shape
    c = p / n

    X = (data - data.mean(axis=0)) / sd_full
    X = X.T  # now this is (p,n) convention

    # Power of the spike
    vecmu = X[:,:n0].mean(axis=1)
    ell = sp.linalg.norm(vecmu)**2
    return X, n0, n1, ell

def disp_eigs(ax, eigvals, u, eB, eS, c0, n0, ells, b=1, vM=None):
    """Plot in the first Fig. the sample eigvals distributions with the (limiting)
    population spikes, and in the seconf Fig the principal sample and population
    eigenvector
    """

    n = len(eigvals)

    # disp eigvals hist
    sns.histplot(
        eigvals.flatten(),  # color="blue", cbar_kws={'edgecolor': 'darkblue'},
        stat="density",
        ax=ax[0],
    )

    # disp limiting empirical spectrum density
    xmin = min(np.min(eigvals) * 0.8, np.min(eigvals) * 1.2)  # accounting negative min
    xmax = np.max(eigvals) * 1.2
    xs, density = lsd(eB, eS, c0, b, xmin=xmin, xmax=xmax, nsamples=400)
    ax[0].axes.plot(xs, density, "r", label="limiting density")

    # disp spike eigvals
    if np.isscalar(ells):
        ells = np.array([ells])
    nells = len(ells)  # how many spikes to disp?

    yoffset = np.min((2, ax[0].axes.get_ylim()[1])) * 0.004
    isolated_eigs = np.zeros(ells.shape)
    for i, ell in enumerate(ells):
        isolated_eig = spike(eB, eS, c0, ell, b=b)[0]

        lablim = ""
        labsamp = ""
        if i == 0:
            lablim = r"limiting spikes"
            labsamp = r"largest sample eigvals"

        ax[0].axes.plot(
            isolated_eig,
            yoffset,
            "og",
            fillstyle="none",
            markersize=10,
            label=lablim,
        )
        ax[0].axes.plot(eigvals[-1 - i], yoffset, "ob", label=labsamp)

    ax[0].axes.legend()
    ax[0].axes.set_xlim([xmin, xmax])
    ax[0].axes.set_ylabel("")

    # disp principal spike eigenvector
    if vM is not None:
        zeta = spike(eB, eS, c0, np.max(ells), b)[1]  # alignement index
    else:
        zeta = 1 / np.sqrt(2)  # n0 == n1 case
        vM = np.array([[0, 0], [-1, 1]])

    u_gt = np.concatenate(
        [
            -vM[1, 0] * np.ones(n0) / np.sqrt(n0),
            -vM[1, 1] * np.ones(n - n0) / np.sqrt(n - n0),
        ]
    ) * np.sqrt(
        zeta
    )  # ground truth

    ax[1].plot(u, "k")
    ax[1].plot(u_gt, "r")


def disp_eigs_full(axes, eigvals, u, c0, ell, lmax= 15, b=1):
    """Plot in the first Fig. the truncated sample eigvals distributions of the
    non-punctured kernel, and in the second Fig the dominanr sample and population
    eigenvector
    """
    isolated_eig = spike(1, 1, c0, ell, b=1)[0]

    spike_eig = [isolated_eig, eigvals[-1]]
    in_xmax = np.max(spike_eig) * 2
    in_xmin = np.min(spike_eig) * 0.5
    # sns.lineplot(x=[in_xmin, in_xmax], y=[0, 0], lw=2, color="k", ax=axins)

    axes[0].axes.plot([in_xmin, in_xmax], [0, 0], "-k")
    axes[0].axes.plot(
        isolated_eig,
        0,
        "og",
        fillstyle="none",
        markersize=10,
        label=r"limiting spike",
    )
    axes[0].axes.plot(eigvals[-1], 0, "ob", markersize=8, label=r"Largest eigval")

    eigvals_t = eigvals[eigvals < lmax]

    axes[0].axes.hist(eigvals_t, bins=np.linspace(0, lmax, 50), density=True)
    axr, density = lsd(1, 1, c0, b=1, xmin=0, xmax=lmax, nsamples=400)

    axes[0].axes.plot(axr, density, lw=2, color="r", label="limiting density")
    axes[0].axes.set_ylabel("")
    axes[0].axes.set_xlim([0, lmax])
    axes[0].axes.set_ylim([0, 0.1])
    print("Full case: largest sample eigval == {:.2f}, lagest limiting spike == {:.2f}"
       .format(eigvals[-1], isolated_eig))

    # data = {'eig': eigvals[-1] , 'y': 0}
    # data = pd.DataFrame([data])
    # sns.scatterplot(data=data, x='eig', y='y', marker="o",, ax=axins)

    u_gt = np.ones(len(u))
    n0 = len(u) // 2
    u_gt[n0:] = -1
    u_gt = u_gt / np.sqrt(len(u_gt))

    axes[1].axes.plot(u, 'k')
    sns.lineplot(x=np.arange(len(u_gt)), y=u_gt, lw=2, color="r", ax=axes[1])
    axes[1].axes.set_ylim([-0.05, 0.05])



def get_perf_clustering(n0=5000, nbMC=10, isGAN=False):
    """Compute the missclassification rates for two-way punctured
    spectral clustering as a function of the puncturing rates
    for 'MNIST' fashion (default choice) or 'GAN' datasets
    """

    s_eps_ref = 0.1
    b_eps_ref = 0.1
    index_ref = s_eps_ref ** 2 * b_eps_ref

    # Get the two classes GAN, or MNIST, vectors
    if (isGAN):
        X, n0, n1, ell = gen_GAN_vectors(n0)
        # Set the list of epsilon_B puncturing rates
        b_eps_vals = np.array(
            [
                5e-4, 0.001, 0.005, 0.01, 0.012, 0.015, 0.02, 0.03, 0.05, 0.1,
                0.5, 1,
            ]
        )
    else:
        X, n0, n1, ell = gen_MNIST_vectors(n0)
        # Set the list of epsilon_B puncturing rates
        b_eps_vals = np.array(
            [
                5e-4, 0.001, 0.002, 0.003, 0.004, 0.005, 0.01, 0.012, 0.015,
                0.02, 0.03, 0.05, 0.1, 0.5, 1,
            ]
        )
    (p, n) = X.shape
    c = p / n

    neps = len(b_eps_vals)

    # Arrays for empirical clustering perf
    fixed_s_eps_perf = np.zeros((neps))
    equiv_s_eps_perf = np.zeros((neps))
    # Set the epsilon_S puncturing rates in the equi-performance regime
    perf_ref = np.sqrt( s_eps_ref ** 2 * b_eps_ref / c)
    s_eps_equi_vals = perf_ref / np.sqrt(b_eps_vals / c)

    # Ground truth vector with class0 and class1 coded as +1 and -1 resp.
    u_gt = np.ones((n, 1))
    u_gt[n0:, 0] = -1
    for iMC in range(nbMC):
        # progression bar
        sys.stdout.write("\r")
        sys.stdout.write("[progression {}/{}]".format(iMC + 1, nbMC))
        sys.stdout.flush()

        # Fixed epsilon_S data
        X_S_fix = puncture_X(X, s_eps_ref)  # fixed eps_S regime
        # 'Brute force' implementation: use dense vectors to get numpy multithreading and optimization
        X_S_fix = X_S_fix.todense()
        K_full_fix = X_S_fix.T @ X_S_fix

        for i, b_eps in enumerate(b_eps_vals):
            B, _, _ = mask_B(n, b_eps)

            # Fixed S perf
            K_fix = sp.sparse.csr_matrix(B.multiply(K_full_fix)) / p
            K_fix = K_fix + sp.sparse.tril(K_fix, k=-1).T
            _, u = sp.sparse.linalg.eigsh(
                K_fix, k=1, which="LA", tol=0, return_eigenvectors=True
            )
            m0 = np.mean(u[:n0])
            m1 = np.mean(u[n0:])
            u = u * np.sign(m0 - m1)
            fixed_s_eps_perf[i] = np.mean(u * u_gt < 0)

            # Equiperf eps_S regime
            s_eps_eq = s_eps_equi_vals[i]
            X_S_eq = puncture_X(X, s_eps_eq)  # equi-performance regime
            K_eq = puncture_K_vanilla(X_S_eq, B)
            _, u = sp.sparse.linalg.eigsh(
                K_eq, k=1, which="LA", tol=0, return_eigenvectors=True
            )
            m0 = np.mean(u[:n0])
            m1 = np.mean(u[n0:])
            u = u * np.sign(m0 - m1)
            equiv_s_eps_perf[i] = np.mean(u * u_gt < 0)

        d = {
            "Beps": b_eps_vals,
            "Seps_equi": s_eps_equi_vals,
            "perf_equi": equiv_s_eps_perf,
            "perf_fixed": fixed_s_eps_perf,
            "nbMC": nbMC,
            "n0": n0,
            "n1": n1,
        }
        if iMC > 0:
            df_tmp = pd.DataFrame(data=d)
            df = df.append(df_tmp, ignore_index=True)
        else:
            df = pd.DataFrame(data=d)
    return df, n0


def plot_perf_clustering(df, ax, isGAN=False):
    """Plot the missclassification rates for two-way punctured
    spectral clustering as a function of the puncturing rates
    """

    s_eps_ref = 0.1
    b_eps_ref = 0.1
    index_ref = s_eps_ref ** 2 * b_eps_ref

    sns.lineplot(
        data=df,
        x="Beps",
        y="perf_equi",
        err_style="band",
        ci="sd",
        label=r"$\epsilon_S^2 \epsilon_B=${:.3f}".format(index_ref),
        ax=ax,
    )
    sns.lineplot(
        data=df,
        x="Beps",
        y="perf_fixed",
        err_style="band",
        ci="sd",
        label=r"$\epsilon_S=${:.3f}".format(s_eps_ref),
        ax=ax,
    )
    ax.axes.legend()
    ax.axes.set_ylabel("")
    if (isGAN):
        ax.axes.set_ylim([0, 0.1])
    else:
        ax.axes.set_ylim([0, 0.4])
    ax.axes.set_xlim([0, 0.1])
    ax.axes.grid("on")



# Define below the useful mathematical functions introduced in the paper

def qfunc(t):
    return 0.5 - 0.5 * scipy.special.erf(t / np.sqrt(2))

def coeffs_F(eB, eS, c0):
    return [1, 2 / eS, 1 / eS ** 2 * (1 - c0 / eB), -2 * c0 / eS ** 3, -c0 / eS ** 4]


def F(eB, eS, c0, t):
    return np.polyval(coeffs_F(eB, eS, c0), t)


def G(eB, eS, c0, b, t):
    return (
        eS * b
        + 1 / c0 * eB * eS * (1 + eS * t)
        + eS / (1 + eS * t)
        + eB / t / (1 + eS * t)
    )


def spike(eB, eS, c0, ell, b=1):
    Gamma = np.max(np.roots(coeffs_F(eB, eS, c0)))

    rho = int(ell > Gamma) * G(eB, eS, c0, b, ell)
    zeta = int(ell > Gamma) * (F(eB, eS, c0, ell) * eS ** 3 / ell / (1 + eS * ell) ** 3)

    return rho, zeta


def phase_transition(c0, ell, res=1e-3):
    eBs = np.arange(res, 1, res)
    eSs = np.zeros(len(eBs))

    for iB, eB in enumerate(eBs):
        eSs[iB] = res
        while np.max(np.roots(coeffs_F(eB, eSs[iB], c0))) > ell and eSs[iB] < 1:
            eSs[iB] += res

    return eBs, eSs

def lsd(eB, eS, c, b=1, xmin=-5, xmax=10, nsamples=1000):
    axr = np.linspace(xmin, xmax, nsamples)
    m_old = 0
    iy = 1e-3
    watchdog = 50000
    density = np.zeros(axr.shape, dtype="float")
    for i, x in enumerate(axr):
        z = (x + iy * 1j) / eS
        m = m_old
        delta = 1
        w = 0
        while (delta > 1e-6) and w < watchdog:
            m_bar = 1 / (b - z - eB / c * m + eB ** 3 * m ** 2 / (c * (c + eB * m)))
            delta = np.abs(m - m_bar)
            m = m_bar
            w += 1
        density[i] = (m_bar / eS).imag / np.pi
    return axr, density
